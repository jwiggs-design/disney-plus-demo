# Disney+ Demo
### Installation Instructions
* Download source code to local repository
* Open index.html file in modern browser (preferably Chrome or Firefox)
* If images are not loading try Incognito/Private Browsing mode in case of CORS issues from running app locally

### Navigating Around the Application
* Use arrow keys (or Tab and Shift/Tab) to navigate between tiles
* For tiles with preview video art, press enter while desired tile is focused to view preview. Press any key to return to tile grid

### Future Enhancements
* Improve asynchronous flow of initial content requests
* Add loading screen to provide smoother experience
* Add more responsive styling using media queries and em or % instead of px
* Improve error handling for image(s) that aren't returned properly
* Add additional information about the title during preview mode (after selecting Enter on focused tile)
* Fix minor UX defects experienced while navigating between tiles
* General refactoring
