//Global object to maintain application state
window.DisneyPlusDemo = {
  badURL: 'https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/E7BFFF8CD4E7BA85BFB3439CDF90698213E8134E4CC729A9AFA17A2E1FC665D31/scale?format=jpeg&quality=90&scalingAlgorithm=lanczos3&width=500',
  contentRendered: false,
  focusedTile: null,
  nextTile: null,
  previousTile: null,
  rowIndex: 0,
  selectedTile: false,
  tileGrid: []
};

//if tile has a valid url for a background image it is retrieved and returned to build the tile
function getTileURL(item, columnIndex, rowIndex) {
  let backgroundURL = null;
  if (item.image && item.image.tile && item.image.tile['1.78']) {
    const itemTile = item.image.tile['1.78'];
    if (itemTile.series) {
      backgroundURL = itemTile.series.default.url;
    }
    else if (itemTile.set) {
      backgroundURL = itemTile.set.default.url;
    }
    else if (itemTile.program) {
      backgroundURL = itemTile.program.default.url;
    }
    else if (itemTile.default) {
      backgroundURL = itemTile.default.default.url;
    }
  }
  else {
    console.log('no image found for tile at column ' + columnIndex + ' row ' + rowIndex);
  }
  return backgroundURL;
}

//if title has video art it is safely retrieved
function getTileVideoURL(item) {
  if (item.videoArt && item.videoArt[0] && item.videoArt[0].mediaMetadata.urls[0]) {
      return item.videoArt[0].mediaMetadata.urls[0].url;
  }
  else {
    return null;
  }
}

//builds a row based on data from home.json or ref request and adds it to the DOM
function renderContentRow(items, rowTitle, rowIndex) {
  window.DisneyPlusDemo.rowIndex = window.DisneyPlusDemo.rowIndex + 1;
  window.DisneyPlusDemo.tileGrid[rowIndex] = [];
  let columnIndex = 0;
  let newRow = '<h1 class="row-title">' + rowTitle + '</h1><div class="row-wrapper" tabindex="-1"><div class="row" id="row' + rowIndex + '" tabindex="-1">';
  for (let item of items) {
    const tileURL = getTileURL(item, columnIndex, rowIndex);
    const tileVideoURL = getTileVideoURL(item);
    if (tileURL != null && tileURL != window.DisneyPlusDemo.badURL) {
      newRow += '<img src="' + tileURL + '" class="tile" id="column' + columnIndex + 'row' + rowIndex + '" tabindex="0"></img>';
      window.DisneyPlusDemo.tileGrid[rowIndex].push(tileVideoURL);
      columnIndex++;
    }
    else {
      console.log('unable to build tile for column ' + columnIndex + ' row ' + rowIndex);
    }
  }
  newRow += '</div></div>';

  if (columnIndex > 0) {
    document.getElementById('tilegrid').innerHTML += newRow;
    return true;
  }
  else {
    console.log('no columns added for row ' + rowIndex);
    return false;
  }
}

//makes request for refs found in home.json and builds row from data retrieved
function loadDynamicRefSet(refId, rowTitle) {
  fetch('https://cd-static.bamgrid.com/dp-117731241344/sets/' + refId + '.json', { method: 'GET' })
  .then(response => {
    response.json().then(data => {
      let items = null;
      if (data.data.CuratedSet && data.data.CuratedSet.items) {
        items = data.data.CuratedSet.items;
      }
      else if (data.data.TrendingSet && data.data.TrendingSet.items) {
        items = data.data.TrendingSet.items;
      }
      else if (data.data.PersonalizedCuratedSet && data.data.PersonalizedCuratedSet.items) {
        items = data.data.PersonalizedCuratedSet.items;
      }
      else {
        console.log('check the key name for: ' + refId);
      }

      renderContentRow(items, rowTitle, window.DisneyPlusDemo.rowIndex);
    })
  })
  .catch(error => {
    console.log(error);
  });
}

//begins building tile grid using data retrieved from home.json
function loadStaticContent() {
  fetch('https://cd-static.bamgrid.com/dp-117731241344/home.json', { method: 'GET' })
    .then(response => {
      response.json().then(data => {
        for (let container of data.data.StandardCollection.containers) {
          const rowTitle = container.set.text.title.full.set.default.content;
          if (container.set && container.set.items) {
            renderContentRow(container.set.items, rowTitle, window.DisneyPlusDemo.rowIndex);
          }
          else if (container.set && container.set.refId) {
            loadDynamicRefSet(container.set.refId, rowTitle);
          }
          else {
            console.log('no items found for row ' + window.DisneyPlusDemo.rowIndex);
          }
        }
        window.DisneyPlusDemo.contentRendered = true;
      })
    })
    .catch(error => {
      console.log(error);
    });
}

loadStaticContent();
