//helper for "forward" right/down/tab key events
function focusForwardHelper() {
  window.DisneyPlusDemo.focusedTile.blur();
  window.DisneyPlusDemo.nextTile.focus();
  window.DisneyPlusDemo.previousTile = window.DisneyPlusDemo.nextTile.previousSibling;
  window.DisneyPlusDemo.focusedTile = window.DisneyPlusDemo.nextTile;
  window.DisneyPlusDemo.nextTile = window.DisneyPlusDemo.focusedTile.nextSibling;
}

//handles tab/right arrow event
function focusRight() {
  if (window.DisneyPlusDemo.nextTile) {
    focusForwardHelper();
  }
  else {
    const nextRow = parseInt(window.DisneyPlusDemo.focusedTile.id.split('row')[1]) + 1;
    if (nextRow <= window.DisneyPlusDemo.tileGrid.length - 1) {
      window.DisneyPlusDemo.nextTile = document.getElementById('column0row' + nextRow);
      focusForwardHelper();
    }
    else {
      window.DisneyPlusDemo.nextTile = document.getElementById('column0row0');
      focusForwardHelper();
    }
  }
}

//helper for "backwards" left/up/shift+tab key events
function focusBackHelper() {
  window.DisneyPlusDemo.focusedTile.blur();
  window.DisneyPlusDemo.previousTile.focus();
  window.DisneyPlusDemo.nextTile = window.DisneyPlusDemo.previousTile.nextSibling;
  window.DisneyPlusDemo.focusedTile = window.DisneyPlusDemo.previousTile;
  window.DisneyPlusDemo.previousTile = window.DisneyPlusDemo.previousTile.previousSibling;
}

//handles shift+tab/left arrow event
function focusLeft() {
  if (window.DisneyPlusDemo.previousTile) {
    focusBackHelper();
  }
  else {
    let previousRow = parseInt(window.DisneyPlusDemo.focusedTile.id.split('row')[1]) - 1;
    if (previousRow < 0) {
      previousRow = window.DisneyPlusDemo.tileGrid.length - 1;
    }
    const lastTile = window.DisneyPlusDemo.tileGrid[previousRow].length - 1;
    window.DisneyPlusDemo.previousTile = document.getElementById('column' + lastTile + 'row' +  previousRow);
    focusBackHelper();
  }
}

//handles up arrow event
function focusUp() {
  let aboveColumn = parseInt(window.DisneyPlusDemo.focusedTile.id.split('row')[0].split('column')[1]);
  let aboveRow = parseInt(window.DisneyPlusDemo.focusedTile.id.split('row')[1]) - 1;

  if (aboveRow < 0) {
    aboveRow = window.DisneyPlusDemo.tileGrid.length - 1;
  }

  if (aboveColumn > window.DisneyPlusDemo.tileGrid[aboveRow].length - 1) {
    aboveColumn = window.DisneyPlusDemo.tileGrid[aboveRow].length - 1;
  }
  window.DisneyPlusDemo.previousTile = document.getElementById('column' + aboveColumn + 'row' + aboveRow);
  focusBackHelper();
}

//handles down arrow event
function focusDown() {
  let belowColumn = parseInt(window.DisneyPlusDemo.focusedTile.id.split('row')[0].split('column')[1]);
  let belowRow = parseInt(window.DisneyPlusDemo.focusedTile.id.split('row')[1]) + 1;

  if (belowRow >= window.DisneyPlusDemo.tileGrid.length) {
    belowRow = 0;
  }

  if (belowColumn > window.DisneyPlusDemo.tileGrid[belowRow].length - 1) {
    belowColumn = window.DisneyPlusDemo.tileGrid[belowRow].length - 1;
  }

  window.DisneyPlusDemo.nextTile = document.getElementById('column' + belowColumn + 'row' + belowRow);
  focusForwardHelper();
}

//gets video url from current focused tile and updates the dom to play video
//otherwise hides preview
function handleTileSelect() {
  if (window.DisneyPlusDemo.selectedTile) {
    document.getElementById('tilegridwrapper').style.opacity = '1';
    document.getElementById('tilepreview').src = '';
    window.DisneyPlusDemo.selectedTile = false;
  }
  else {
    const column = parseInt(window.DisneyPlusDemo.focusedTile.id.split('row')[0].split('column')[1]);
    const row = parseInt(window.DisneyPlusDemo.focusedTile.id.split('row')[1]);
    const videoURL = window.DisneyPlusDemo.tileGrid[row][column];

    if (videoURL) {
      document.getElementById('tilegridwrapper').style.opacity = '0';
      document.getElementById('tilepreview').src = videoURL;
      window.DisneyPlusDemo.selectedTile = true;
    }
  }
}

//handler for keydown events
//allows user to navigate using arrow keys, tab/shift+tab, and enter to view previews when available
function handleKeyDown(event) {
  if (window.DisneyPlusDemo.selectedTile) {
    event.preventDefault();
    handleTileSelect();
  }
  else {
    switch (event.code) {
      case 'ArrowRight':
        focusRight();
        break;
      case 'ArrowLeft':
        focusLeft();
        break;
      case 'ArrowUp':
        focusUp();
        break;
      case 'ArrowDown':
        focusDown();
        break;
      case 'Tab':
        event.preventDefault();
        if (event.shiftKey) {
          focusLeft();
        }
        else {
          focusRight();
        }
        break;
      case 'Enter':
        event.preventDefault();
        handleTileSelect();
      default:
        break;
    }
  }
}

//focuses on first tile and sets up event listener for key down events to enable navigation
function initializeNavigation() {
  clearInterval(interval);

  window.DisneyPlusDemo.focusedTile = document.getElementById('column0row0');
  window.DisneyPlusDemo.focusedTile.focus();
  window.DisneyPlusDemo.nextTile = window.DisneyPlusDemo.focusedTile.nextSibling;

  const lastRow = window.DisneyPlusDemo.tileGrid.length - 1;
  const lastColumn = window.DisneyPlusDemo.tileGrid[lastRow].length - 1;

  window.DisneyPlusDemo.previousTile = document.getElementById('column' + lastColumn + 'row' + lastRow);
  document.addEventListener('keydown', handleKeyDown);
}

//waits for DOM elements to be rendered before setting up navigation
const interval = setInterval(() => {
  if (window.DisneyPlusDemo.contentRendered) {
    initializeNavigation();
  }
  else {
    console.log('waiting');
  }
}, 100);
